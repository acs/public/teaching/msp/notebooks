# Modeling and Simulation of Complex Power Systems

On this platform you find the simulation examples based on Jupyter Notebooks. They are presented in the course **Modeling and Simulation of Complex Power Systems** by **Prof. Monti** at the **RWTH Aachen University**. Reviewing and modifying the simulation examples shall support the students in understanding the lecture content.  

The currently available notebooks are listed in the [index](./index.ipynb) along with some general information on working with Jupyter Notebooks.